package com.fromvc

import pureconfig._
import pureconfig.generic.semiauto._
import zio.{ Has, Layer, ZIO, ZLayer }

object config {

  final case class Config(
    appConfig: AppConfig,
    dbConfig: DbConfig)

  object Config {
    implicit val convert: ConfigConvert[Config] = deriveConvert
  }

  type ConfigProvider = Has[Config]

  object ConfigProvider {

    val live: Layer[IllegalStateException, ConfigProvider] =
      ZLayer.fromEffect(
        ZIO
          .fromEither(ConfigSource.default.load[Config])
          .mapError(
            failures =>
              new IllegalStateException(
                s"Error loading configuration: $failures"
              )
          )
      )
  }

  type DbConfigProvider = Has[DbConfig]

  object DbConfigProvider {

    val fromConfig: ZLayer[ConfigProvider, Nothing, DbConfigProvider] =
      ZLayer.fromService(_.dbConfig)
  }

  type AppConfigProvider = Has[AppConfig]

  object AppConfigProvider {

    val fromConfig: ZLayer[ConfigProvider, Nothing, AppConfigProvider] =
      ZLayer.fromService(_.appConfig)
  }

  final case class AppConfig(
    port: Int,
    baseUrl: String)

  object AppConfig {
    implicit val convert: ConfigConvert[AppConfig] = deriveConvert
  }

  final case class DbConfig(
    url: String,
    driver: String,
    user: String,
    password: String)

  object DbConfig {
    implicit val convert: ConfigConvert[DbConfig] = deriveConvert
  }
}
