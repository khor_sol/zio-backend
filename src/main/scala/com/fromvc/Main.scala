package com.fromvc

import com.fromvc.config.ConfigProvider
import com.fromvc.http.TodoService
import com.fromvc.repository.TodoRepository
import org.http4s.HttpApp
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import zio._
import zio.clock.Clock
import zio.interop.catz._

object Main extends App {
  type AppTask[A] = RIO[Layers.AppEnv with Clock, A]

  override def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] = {
    val prog =
      for {
        cfg    <- ZIO.access[ConfigProvider](_.get)
        _      <- logging.log.info(s"Starting with $cfg")
        appCfg = cfg.appConfig

        httpApp = Router[AppTask](
          "/todos" -> TodoService.routes(s"${appCfg.baseUrl}/todos")
        ).orNotFound

        _ <- runHttp(httpApp, appCfg.port)
      } yield ExitCode.success

    prog
      .provideSomeLayer[ZEnv](TodoRepository.withTracing(Layers.live.appLayer))
      .orDie
  }

  import cats.effect.ExitCode

  def runHttp[R <: Clock](
    httpApp: HttpApp[RIO[R, *]],
    port: Int
  ): ZIO[R, Throwable, Unit] = {
    type Task[A] = RIO[R, A]
    ZIO.runtime[R].flatMap { implicit rts =>
      BlazeServerBuilder[Task](rts.platform.executor.asEC)
        .bindHttp(port, "0.0.0.0")
        .withHttpApp(CORS(httpApp))
        .serve
        .compile[Task, Task, ExitCode]
        .drain
    }
  }
}
