package com.fromvc.repository

import com.fromvc._
import zio.{ Ref, UIO, ZIO, ZLayer }

final class InMemoryTodoRepository(
  ref: Ref[Map[TodoId, TodoItem]],
  counter: Ref[Long]) {

  val todoRepository: TodoRepository.Service = new TodoRepository.Service {

    def getAll: UIO[List[TodoItem]] =
      ref.get.map(_.values.toList)

    def getById(id: TodoId): UIO[Option[TodoItem]] =
      ref.get.map(_.get(id))

    def delete(id: TodoId): UIO[Unit] =
      ref.update(_ - id)

    def deleteAll: UIO[Unit] =
      ref.update(_.empty)

    def create(todoItemForm: TodoItemPostForm): UIO[TodoItem] =
      for {
        newId <- counter.updateAndGet(_ + 1).map(TodoId)
        todo  = todoItemForm.asTodoItem(newId)
        _     <- ref.update(_ + (newId -> todo))
      } yield todo

    def update(
      id: TodoId,
      todoItemForm: TodoItemPatchForm
    ): UIO[Option[TodoItem]] =
      for {
        oldValue <- getById(id)
        result <- oldValue.fold[UIO[Option[TodoItem]]](ZIO.succeed(None)) { x =>
                   val newValue = x.update(todoItemForm)
                   ref.update(_ + (id -> newValue)) *> ZIO.succeed(
                     Some(newValue)
                   )
                 }
      } yield result
  }
}

object InMemoryTodoRepository {

  val layer: ZLayer[Any, Nothing, TodoRepository] =
    ZLayer.fromEffect {
      for {
        ref     <- Ref.make(Map.empty[TodoId, TodoItem])
        counter <- Ref.make(0L)
      } yield new InMemoryTodoRepository(ref, counter).todoRepository
    }
}
